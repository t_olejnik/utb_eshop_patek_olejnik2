﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Text;
using utb_eshop_patek.Domain.Configuration;
using utb_eshop_patek.Infrastructure.Configuration;

namespace utb_eshop_patek.Application.Configuration
{
    public class ApplicationDependencyModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterAssemblyTypes(ThisAssembly)
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.RegisterModule<DomainDependencyModule>();
            builder.RegisterModule<InfrastructureDependencyModule>();
        }
    }
}
