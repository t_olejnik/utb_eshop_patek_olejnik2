﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using utb_eshop_patek.Application.Admin.ViewModels.Security;
using utb_eshop_patek.Infrastructure.Identity.Users;

namespace utb_eshop_patek.Application.Admin.ApplicationServices.Security
{
    public interface ISecurityApplicationService
    {
        Task<bool> Login(LoginViewModel viewModel);
        Task Logout();
        Task RegisterAndLogin(LoginViewModel viewModel);
        Task<User> GetCurrentUser(ClaimsPrincipal principal);
        Task<User> GetUserByEmail(string email);
        Task<IList<string>> GetUsersRoles(User user);
    }
}
