﻿using System;
using System.Collections.Generic;
using System.Text;

namespace utb_eshop_patek.Domain.Constants
{
    public class Roles
    {
        public const string User = "User";
        public const string Admin = "Admin";
        public const string Manager = "Manager";
    }
}
