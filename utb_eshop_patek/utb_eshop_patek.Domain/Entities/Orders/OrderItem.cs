﻿using utb_eshop_patek.Domain.Entities.Products;

namespace utb_eshop_patek.Domain.Entities.Orders
{
    public class OrderItem : Entity
    {
        public int OrderID { get; set; }
        public int ProductID { get; set; }

        public Order Order { get; set; }
        public Product Product { get; set; }
    }
}