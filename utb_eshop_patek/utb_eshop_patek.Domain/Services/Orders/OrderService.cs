﻿using System;
using System.Collections.Generic;
using System.Text;
using utb_eshop_patek.Domain.Repositories.Orders;

namespace utb_eshop_patek.Domain.Services.Orders
{
    public class OrderService : IOrderService
    {
        private readonly IOrderRepository _orderRepository;

        public OrderService(IOrderRepository orderRepository)
        {
            _orderRepository = orderRepository;
        }

        public void CreateOrder(int userID, string userTrackingCode)
        {
            _orderRepository.CreateOrder(userID, userTrackingCode);
        }
    }
}
