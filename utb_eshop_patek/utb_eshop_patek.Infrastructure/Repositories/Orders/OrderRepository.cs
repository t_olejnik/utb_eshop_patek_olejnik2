﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using utb_eshop_patek.Domain.Entities.Orders;
using utb_eshop_patek.Domain.Repositories.Carts;
using utb_eshop_patek.Domain.Repositories.Orders;
using utb_eshop_patek.Infrastructure.Data;

namespace utb_eshop_patek.Infrastructure.Repositories.Orders
{
    public class OrderRepository : IOrderRepository
    {
        private readonly ICartRepository _cartRepository;
        private readonly DataContext _dataContext;
        private readonly DbSet<Order> _dbSet;

        public OrderRepository(
            ICartRepository cartRepository,
            DataContext dataContext)
        {
            _cartRepository = cartRepository;
            _dataContext = dataContext;
            _dbSet = _dataContext.Set<Order>();
        }

        public void CreateOrder(int userID, string userTrackingCode)
        {
            var cart = _cartRepository.GetCurrentCart(userTrackingCode);
            var order = new Order
            {
                UserID = userID,
                OrderItems = new List<OrderItem>(),
                OrderNumber = userTrackingCode.Take(8).ToString()
            };
            foreach (var item in cart.CartItems)
            {
                order.OrderItems.Add(new OrderItem
                {
                    ProductID = item.ProductID
                });
            }
            _dbSet.Add(order);
            _dataContext.SaveChanges();
        }

        public IList<OrderItem> GetOrder(int orderID)
        {
            throw new NotImplementedException();
        }

        public IList<Order> GetOrders(int userID)
        {
            throw new NotImplementedException();
        }
    }
}
